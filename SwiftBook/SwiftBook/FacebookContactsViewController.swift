//
//  FacebookContactsViewController.swift
//  SwiftBook
//
//  Created by Click Labs on 2/24/15.
//  Copyright (c) 2015 Brian Coleman. All rights reserved.
//

import UIKit

var taggedFacebookID = [String]()

class FacebookContactsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
  @IBOutlet weak var tagTable: UITableView!
  var selectedRowIndex = [Int]()
  var all = 0  //counter intialized to tag all
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return facebookIDArray.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
    // storing cell with the contents of table
    cell.textLabel?.numberOfLines = 0
    cell.textLabel?.text = facebookIDPersonNameArray[indexPath.row]
    
    if all == 0 {
      cell.accessoryType = UITableViewCellAccessoryType.None //if not selected tick is not displayed
    } else {
      cell.accessoryType = UITableViewCellAccessoryType.Checkmark //if selected check is displayed
    }
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    let currentCell : UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
    
    if currentCell.accessoryType == UITableViewCellAccessoryType.Checkmark {
      currentCell.accessoryType = UITableViewCellAccessoryType.None
      for var i = 0; i < taggedFacebookID.count; i++ {
        
        if taggedFacebookID[i] == facebookIDArray[indexPath.row] {
          taggedFacebookID.removeAtIndex(i)
        }
      } //if users deselcts previously selected cell
    } else {
      currentCell.accessoryType = UITableViewCellAccessoryType.Checkmark
      taggedFacebookID.append(facebookIDArray[indexPath.row])
    }
    println(taggedFacebookID)
  }
  
  @IBAction func tagAll(sender: AnyObject) {
    
    if all == 0 {
      all = 1
      taggedFacebookID = facebookIDArray
    } else {
      all = 0
      taggedFacebookID = []
    }
    tagTable.reloadData()
  }
} //on press everyone in list is tagged and deselected when pressed again
