//
//  FirstViewController.swift
//  SwiftBook
//
//  Created by Click Labs on 2/24/15.
//  Copyright (c) 2015 Brian Coleman. All rights reserved.
//  Make sure the phone have facebook contact integration on

import UIKit
import MobileCoreServices

var pickedImage = UIImage()
var caption = ""

class FirstViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate,UITextFieldDelegate {
  
  @IBOutlet weak var openGalleryPressed: UIButton!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var captionText: UITextField!
  var picker:UIImagePickerController? = UIImagePickerController()
  var popover:UIPopoverController? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    captionText.text = ""
    picker?.delegate = self
    captionText.delegate = self
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  @IBAction func openCameraClicked(sender: AnyObject) {
    self.openCamera()
  }
  
  @IBAction func openGalleryClicked(sender: AnyObject) {
    self.openGallery()
  }
  
  func openCamera()
  {
    if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
    {
      picker!.sourceType = UIImagePickerControllerSourceType.Camera
      self .presentViewController(picker!, animated: true, completion: nil)
    }
    else
    {
      openGallery()
    }
  }
  
  func openGallery()
  {
    picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
    if UIDevice.currentDevice().userInterfaceIdiom == .Phone
    {
      self.presentViewController(picker!, animated: true, completion: nil)
    }
    else
    {
      popover=UIPopoverController(contentViewController: picker!)
      popover!.presentPopoverFromRect(openGalleryPressed.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
    }
  }
  
  func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]!)
  {
    picker .dismissViewControllerAnimated(true, completion: nil)
    pickedImage  = info[UIImagePickerControllerOriginalImage] as UIImage
    imageView.image = pickedImage
    //sets the selected image from gallery to image view
  }

  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    captionText.endEditing(true)
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  @IBAction func shareButtonClicked(sender: AnyObject) {
    caption = captionText.text
  }
}
