//
//  ViewController.swift
//  SwiftBook
//
//  Created by Brian Coleman on 2014-07-07.
//  Copyright (c) 2014 Brian Coleman. All rights reserved.
//

import UIKit
import AddressBook
import AddressBookUI

var facebookIDArray  = [String]()
var facebookIDPersonNameArray  = [String]()

class ViewController: UIViewController, FBLoginViewDelegate {
  
  var counter = 0
  var tagList = [String]()
  @IBOutlet var fbLoginView : FBLoginView!
  @IBOutlet weak var tagImage: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    facebookIDArray = []
    facebookIDPersonNameArray = []
    
    tagContacts()
    
    self.fbLoginView.delegate = self
    self.fbLoginView.readPermissions = ["public_profile", "email", "user_friends", "publish_stream","user_photos","publish_actions"] //requesting for these permissions mandatory
    tagList = ["1377311125919484"]
    tagImage.image = pickedImage
    println(taggedFacebookID)
    counter = 0
  }
  
  // Facebook Delegate Methods
  
  func loginViewShowingLoggedInUser(loginView : FBLoginView!) {
    println("User Logged In")
  }
  
  func loginViewFetchedUserInfo(loginView : FBLoginView!, user: FBGraphUser) {
    //Getting users data
    println("User: \(user)")
    println("User ID: \(user.objectID)")
    println("User Name: \(user.name)")
    var userEmail = user.objectForKey("email") as String
    println("User Email: \(userEmail)")
  }
  
  func loginViewShowingLoggedOutUser(loginView : FBLoginView!) {
    println("User Logged Out")
  }
  
  func loginView(loginView : FBLoginView!, handleError:NSError) {
    println("Error: \(handleError.localizedDescription)")  //intialize the login button on view
  }
  
  @IBAction func uploadPressed(sender: AnyObject) {
    
    var image : UIImage = pickedImage
    var photoId = ""
    if counter == 0 {
      var params : NSMutableDictionary = NSMutableDictionary(objectsAndKeys: "\(caption)","caption",image, "picture")
      FBRequestConnection.startWithGraphPath("me/photos", parameters: params, HTTPMethod: "POST", completionHandler: {
        (connection,result,error) in
        println(error)
        photoId = result.objectForKey("id") as String
        println(photoId)
        self.tagList = taggedFacebookID
        
        for i in self.tagList {
          FBRequestConnection.startWithGraphPath("\(photoId)/tags/\(i)", parameters: nil, HTTPMethod: "POST", completionHandler: {
            (connection,result,error) in
            println(result)
          }) //loop above tag friends
        }
      }) //photo uploaded with tags
      counter++
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  func tagContacts(){
    let status = ABAddressBookGetAuthorizationStatus()
    if status == .Denied || status == .Restricted {
      // user previously denied, to tell them to fix that in settings
      return
    }
    
    var error: Unmanaged<CFError>? = nil
    let addressBook: ABAddressBookRef? = ABAddressBookCreateWithOptions(nil, &error)?.takeRetainedValue()
    if addressBook == nil {
      println(error?.takeRetainedValue())
      return
    }
    
    // requesting permission to use addressbook
    ABAddressBookRequestAccessWithCompletion(addressBook) {
      (granted, error) in
      
      if !granted {
        // warn the user that because they just denied permission, this functionality won't work
        // also let them know that they have to fix this in settings
        return
      } else {
        let people = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as NSArray
        var totalContacts = people.count
        var firstName = ""
        var lastName = ""
        
        for record:ABRecordRef in people {
          
          if totalContacts != 0 {
            var contactPerson: ABRecordRef = record
            let emailProperty: ABMultiValueRef = ABRecordCopyValue(record, kABPersonEmailProperty).takeRetainedValue() as ABMultiValueRef
            var isEmailEmpty = emailProperty.description.componentsSeparatedByString(" ")[3]
            var fullSocialProfile: ABMultiValueRef = ABRecordCopyValue(record, kABPersonSocialProfileProperty).takeRetainedValue() as ABMultiValueRef
            
            if isEmailEmpty != "0" {
              let allEmailIDs: NSArray = ABMultiValueCopyArrayOfAllValues(emailProperty).takeUnretainedValue() as NSArray
              for i in 0..<allEmailIDs.count {
                let emailID = allEmailIDs[i] as String
              }
            } //contacts are not zero and they have emails present checked
            
            if let contactFirstName = ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty).takeRetainedValue() as? NSString {
              firstName = contactFirstName
              println ("\t\t contactFirstName : \(firstName)")
            }
            
            if let contactLastName = ABRecordCopyValue(contactPerson, kABPersonLastNameProperty).takeRetainedValue() as? NSString {
              println ("\t\t contactLastName : \(contactLastName)")
              lastName = contactLastName
            }
            
            if fullSocialProfile.description.rangeOfString("www.facebook.com/profile.php?id=")?.endIndex != nil {
              var saperateIDString  = fullSocialProfile.description.componentsSeparatedByString("www.facebook.com/profile.php?id=")
              var getIDString = saperateIDString[1].componentsSeparatedByString("\"")[0]
              facebookIDArray.append(getIDString)
              facebookIDPersonNameArray.append(firstName + " " + lastName)
              //getting user id from social profile in contacts
              
            } else if emailProperty.description.rangeOfString("@facebook.com")?.endIndex != nil {
              var saperateIDString  = emailProperty.description.componentsSeparatedByString("@facebook.com")[0]
              var getIDString = saperateIDString.componentsSeparatedByString(" ")
              println(getIDString[getIDString.count - 1])
              facebookIDArray.append(getIDString[getIDString.count - 1])
              facebookIDPersonNameArray.append(firstName + " " + lastName)
              //getting facebook id from email if supplied over there
            }
            totalContacts--
          }
        }
      }
    }
  }
}

